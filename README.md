# Simple Web Examples

This repository contains example projects demonstrating how to use
Simple Web. Requires Simple Web version 0.3.0.

1. [Hello World](examples/hello_world)
2. [Handling Static](examples/static_eg)
3. [Handling Templates](examples/templates_eg)
4. [An API](examples/api)
5. [With Vue.js](examples/vue)
5. [Extensible Templates with Bootstrap](examples/bootstrap_templates)
