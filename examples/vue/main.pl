:- module(main,
    [ serve/0
    , view/0
    ]
).

% Set sw_app to this directory so app can be run from any directory
:- prolog_load_context(directory, Dir),
   asserta(user:file_search_path(sw_app, Dir)).

:- use_module(library(sw/simple_web)).

% For browser opening
:- use_module(library(www_browser)).

% Include our API
:- use_module(api/api).


serve :-
    % port(X) where X must match that in
    % vueapp/config/index.js: proxyTable url
    run(port(8000)), !.

view :-
    serve,
    www_open_url("http://localhost:8000"), !.

% route '/' is only used in production, this will
% return the built index.html, which itself will
% request JavaScript and CSS from static.
sw:route(home, root(.), Request) :-
    reply_file('vueapp/dist/index.html', [cache(true)], Request).
