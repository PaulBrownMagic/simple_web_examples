# Using Simple Web server-side with Vue.js client-side

## Setup Process

First you need Vue.js and Vue CLI, assuming you already have npm. I'm also
using axios to manage the HTTP requests from Vue to the server. With the
installations done, you can use the CLI to init a new webpack templated
application, make sure you install the npm packages when prompted.

```bash
:~$ npm i -g vue @vue/cli axios vue-axios
:-$ vue init webpack vueapp
vueapp:~$ npm install
```

For a smooth development experience, we'll tell our JavaScript to use
the Simple Web API we'll create as a proxy. You'll need to edit
`vueapp/config/index.js`. Under `module.exports` you'll find `dev`,
and under `dev` you'll find `proxyTable:{}`. Change the
`proxyTable:{}` to:

```js
proxyTable {
	'/api': {
		target: 'http://localhost:8000',
		changeOrigin: true
	}
},
```

For production we'll tell Simple Web where to serve the static and
`index.js` from, we tell Simple Web where to find static in
`config.pl`. FYI, if you're following along, these files won't exist yet if you've not built your app.

## Running in Development and Production

This would be a good time to look at `vueapp/src` as well as
`main.pl` and `api.pl`, which consist of our basic application. Note
how I've included axios in `vueapp/src/main.js`. You
can test the development environment by running the SWI-Prolog
server in one terminal and the JavaScript in another:

```bash
:~$ swipl -g [main] -g serve
```

```bash
vueapp:~$ npm run dev
```

You can test the production environment by building the application
and running the SWI-Prolog server. Build the application with:

```bash
vueapp:~$ npm run build
```

I added a little code to
automatically open it in your browser for you when you start the
server.

```bash
:~$ swipl -g [main] -g view
```

Now you have easy development and deployment, just run the npm
server for development, and don't for production. You've also got a
very easy way to declare your API routes using simple web. Enjoy!
