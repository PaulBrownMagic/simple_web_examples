:- module(api,
    [
    ]
).

:- use_module(library(sw/simple_web)).

:- location(api, "/api").

sw:route(message, api(message), method(get), _Request) :-
    reply_json_dict(resp{msg: "Hello world from Simple Web API!"}).
