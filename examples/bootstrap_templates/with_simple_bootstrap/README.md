# Bootstrap Templates Example

This example uses `library(sw/simple_bootstrap)`, minimum version 0.0.2,
which can be installed with:

```
?- pack_install(simple_bootstrap).
```

## As Per DIY

This is not dissimilar from the [DIY Version](../diy), except you'll
notice there's a lot less in the directory structure as much is provided
for you in Simple Bootstrap. This is the "quick start" version.

## Easy Similar Templates

Often we want very similar templates with only a few small
differences, for example we'll want the same navigation, or same styles
or scripts on a bunch of related pages. The example shows one way to
achieve this in the `home_template/2` predicate in `main.pl`. This
accepts a `CustomDict` of unique content and joins it to
`Home`, which is shared content. Using this method you can create
different but similar templates for all your related routes.

## Gotchas Resolved

Using Simple Bootstrap, you can overcome the clobbering issue of
overwriting blocks by re-including them in your templates. For example,
with styles using the provided `url_for` function:

```html
{{ dynamic_include bootstrap("styles") }}
<link href="{{= url_for("static('css/my_styles.css')") }}" rel="stylesheet">
```
