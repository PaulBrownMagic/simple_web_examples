:- module(main,
    [ serve/0
    ]
).

% Assert app dir so it can be run from anywhere
:- prolog_load_context(directory, Dir),
   asserta(user:file_search_path(sw_app, Dir)).

:- use_module(library(sw/simple_web)).
:- use_module(library(sw/simple_bootstrap)).

%! join_dicts(+D1, +D2, -D3) is nondet.
%  Join dicts together if there is no
%  conflict
join_dicts(D1, D2, D3) :-
    D1 >:< D2,
    put_dict(D1, D2, D3).

%! home_template(+CustomDict, -Template) is nondet.
%  Merge custom options into the common home template
%  options.
home_template(CustomDict, Template) :-
    Home = _{ navbar: navbar_eg
            , styles: styles_eg
            },
    join_dicts(CustomDict, Home, Template).

sw:route(home, '/', _Request) :-
    home_template(template{ title: 'Hello Bootstrap'
                          , content: content_eg
                          , content_message: "You can include data for templates too!"
                          }, Template),
    reply_template(bootstrap_base, Template).

serve :-
    run(port(8000)).
