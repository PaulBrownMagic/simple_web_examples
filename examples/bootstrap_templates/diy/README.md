# Bootstrap Templates Example

This example is to show how to invert the dependencies in Simple
Template to create extensible templates from a base. It has been
created using [Bootstrap](https://getbootstrap.com/) version 4.

## The Bootstrap Base Template

We begin with a very generic base template for bootstrap, you can read
it in `templates/bootstrap_base.html`. You'll find lots and lots of
`{{ if some_var }}{{ dynamic_include some_var }}{{end}}` type
statements in there. For this reason the `config.pl` file sets
`st_undefined` to false, so these variables don't have to be set. This
creates an extensible base that we can use to create our
application.

## Easy Similar Templates

Often we want very similar templates with only a few small
differences, for example we'll want the same navigation, or same styles
or scripts on a bunch of related pages. The example shows one way to
achieve this in the `home_template/2` predicate in `main.pl`. This
accepts a `CustomDict` of unique content and joins it to
`Home`, which is shared content. Using this method you can create
different but similar templates for all your related routes.

## Gotchas

In this example, blocks clobber each other. Take a look at
`templates/bootstrap_base.html` and `templates/styles_eg.html`, you'll
see that the link to `bootstrap.min.css` had to be included in
`templates/styles_eg.html` because the inclusion of `styles`
overwrote the automatic inclusion of it. This is a deliberate design
choice to allow maximum flexibility and it could be expanded even
further to the whole head, body and html tags.

## Subtle `favicon.ico` trick

In `templates/bootstrap_base.html` you'll see a `<link>` tag that
defines the source of the `favicon.ico` to be in `static/images`. This
removes the headache of having to declare a route just for that.
