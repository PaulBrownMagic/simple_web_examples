# Bootstrap Templates

In this directory there are two versions.

## DIY

The Do It Yourself version is excellent to see how to create extensible
templates using dicts.

## With Simple Bootstrap

Uses a library pack for a super quick start. Provides the functionality
of DIY and a little more. However, as it uses a library pack, the base
template and other Bootstrap static is not in this directory.
