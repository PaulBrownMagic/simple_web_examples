# Serving Static

Static files are those that are served with as files, for example:

* Images
* CSS
* JavaScript
* Icons
* Fonts

Often a web server (NGINX or Apache) will directly handle static
files for speed of response. However, sometimes, especially during
development, it's useful to let Prolog serve these files too. Simple
Web provides two ways to do this.

## Built-in Static Handling

Make a directory called static at the root of your application.
anything you put into this directory is served at the matching
route. For example `static/images/hedgehog.jpg` is at the matching route
`/static/images/hedgehog.jpg`. You can use this for CSS and
JavaScript too:

```
static
├── css
│   └── styles.css
├── favicon.ico
├── fonts
│   └── roboto.ttf
├── images
│   ├── logo.jpg
│   ├── hedgehog.jpg
│   ├── squirell.jpg
│   └── red_kite.jpg
└── js
    └── main.js
```

With this structure you can include tags in your HTML such as:

```
<link rel="icon" href="/static/favicon.ico" type="image/x-icon">
<link href="/static/css/styles.css" rel="stylesheet">
<script type="text/javascript" src="/static/js/main.js">
<img src="/static/images/hedgehog.jpg" alt="hedgehog">
```

**Don't want to name the directory `static`?** Just add
`config(static_dir, DirectoryName)` to a file called `config.pl` in
the root of your project, where `DirectoryName` is the ground name of
your static directory.

## Handling Complex Cases

Sometimes you need more options and more flexibility. That's why
Simple Web also exposes
`[reply_file/3](http://www.swi-prolog.org/pldoc/doc_for?object=http_dispatch%3Ahttp_reply_file/3)` for you, and provides `reply_file/2` without the options. In the example `main.pl` file, we're using this for the '/favicon.ico' route to demonstrate it with caching. Check the linked docs for more information on using this predicate.
