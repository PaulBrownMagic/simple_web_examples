:- module(main,
    [ serve/0
    ]
).

:- use_module(library(sw/simple_web)).

% '/'
%
%  Respond with image from static files
sw:route(home, root(.), _Request) :-
    reply_html("<h1>Serving from static</h1>
                <img src='/static/images/hedgehog.jpg' alt='hedgehog'/>").

% '/favicon.ico'
%
% Respond with image file from static files, cache for speed
sw:route(favicon, root('favicon.ico'), Request) :-
    reply_file('static/images/hedgehog.jpg', [cache(true)], Request).

serve :-
    run(port(8000)).
