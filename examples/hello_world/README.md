# Hello World

**Create your first web page!**

This example shows how to setup a route and reply with
a HTML string. The predicate `serve/0` is used to run
the web server.

## Running from the command line:

```
:~$ swipl -g [hello_world] -g serve
```

## Running from swipl:

```
?- use_module(hello_world).
?- serve.
```
