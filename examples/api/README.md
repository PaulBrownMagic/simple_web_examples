# A Simple Web API

To make creating an API easier, Simple Web has included a
`sw:route/3` and exposed the existing JSON Dict functionality of
`[library(http/http_json)](http://www.swi-prolog.org/pldoc/man?section=jsonsupport)`.

## Setup Your API

First we create a base url for our API with `:- location(Name, Url)`, this
lets us define our API routes with `api/1` as shown with
`api(get_number)`.

## API Methods

You no-longer need to search the `Request` for the method and then
handle it, just put the method into the `sw:route/3`:

```
sw:route(api(somewhere), method(get), _Request) :- ...
```

The POST API route in the example can be tested with curl:

```
:~$ curl --header "Content-Type: application/json" --request POST --data '{"number": "2"}' http://localhost:8000/api/send_number
```

## JSON and Dicts

For more information about the `read_json_dict/2&3` predicates and
`reply_json_dict/1&2` predicates, please refer to the documentation
at `[library(http/http_json)](http://www.swi-prolog.org/pldoc/man?section=jsonsupport)`.
