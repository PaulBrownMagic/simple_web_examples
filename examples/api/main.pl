:- module(main,
    [ serve/0
    ]
).

:- use_module(library(sw/simple_web)).

:- location(api, "/api").

sw:route(home, root(.), _Request) :-
    reply_html("<a href='/api/get_number'>Go To API</a>").

sw:route(get_number, api(get_number), method(get), _Request) :-
    random_between(1, 10, Number),
    reply_json_dict(resp{number: Number, status: success}).

sw:route(send_number, api(send_number), method(post), Request) :-
    read_json_dict(Request, Data),
    NumStr = Data.get(number),
    number_string(Number, NumStr),
    Incr is Number + 1,
    Resp = resp{number: Number,
                incr: Incr,
                status: success},
    reply_json_dict(Resp).

serve :-
    run(port(8000)).
