:- module(blog,
    [ post_titles/1
    , blog_post/2
    ]
).

:- use_module(blog_posts).

post_titles(Titles) :-
    findall(T, blog_post(T, _), Titles).
