# Rendering Templates

Templates are awesome, they let you render HTML on the fly with the
results of your query. Simple Web was inspired by Simple Template and
was built to exploit this pack. But Simple Web is not Simple
Template, so we encourage you to [read their docs](http://www.swi-prolog.org/pack/list?p=simple_template) for a better
understanding of how to use them. This version requires at least version
1.3.0 as it is using the blocks feature.

# Simple Web Templates

Simple web assumes your templates are in a directory called
`templates`, this can be changed by adding to a config.pl file in the
root of your application, where `NewTemplateDir` is the ground name
of your template directory:

```
config(template_dir, NewTemplateDir).
```

# Config Support for Simple Templates

To save you from needing to pass options to `reply_template/3` all
the time, you can configure your own default options in the
`config.pl` file. These options are detailed in the Simple Web docs.

# URL_FOR

Some basic support for getting the URL for a route by name is supported
in the templates. You can see it demonstrated in `templates/post.html`,
where it returns the URL for home. Routes with variables aren't yet
supported with the exception of static URLs, this is demonstrated in
`templates/base.html` where it is used for the favicon.
