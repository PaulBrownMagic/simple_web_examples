:- module(main,
    [ serve/0
    ]
).

:- use_module(library(sw/simple_web)).

% module blog is local and included here
:- use_module(blog,
    [ post_titles/1
    , blog_post/2
    ]).


sw:route(home, '/', _Request) :-
    post_titles(Titles),
    reply_template(blog, data{ titles: Titles
                             , title: 'Blog'
                             }
                  ).

sw:route(post, root(Name), _Request) :-
    blog_post(Name, Content),
    Data = data{ name: Name
               , content: Content
               , title: 'Post'
               },
    reply_template(post, Data).

serve :-
    run(port(8000)).
