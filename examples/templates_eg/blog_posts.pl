:- module(blog_posts,
    [ blog_post/2
    ]
).

%! blog_post(?title, ?content) is semidet.
blog_post('Post One', 'Once upon a time there was a blog.').
blog_post('Post Two', 'It was a very unimaginative blog.').
blog_post('Post Three', 'But it demonstrated templates well').
